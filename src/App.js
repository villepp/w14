import React from "react";
import "./App.css";

function App() {
  const handleSendClick = () => {
    const chatInput = document.getElementById("chat");
    const chatValue = chatInput.value;
    if (chatValue.trim()) {
      const chats = JSON.parse(localStorage.getItem("chats")) || [];
      chats.push(chatValue);
      localStorage.setItem("chats", JSON.stringify(chats));
      chatInput.value = "";
    }
  };

  const handleShowMessagesClick = () => {
    const chats = JSON.parse(localStorage.getItem("chats")) || [];
    console.log(chats);
  };

  return (
    <div className="App">
      <div className="header">
        <h1 className="title">Chat to localStorage assignment</h1>
      </div>
      <div className="lorem-content">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Id
          faucibus nisl tincidunt eget nullam non nisi. Mauris pharetra et
          ultrices neque ornare. Tristique sollicitudin nibh sit amet commodo
          nulla. Morbi tincidunt ornare massa eget egestas purus viverra
          accumsan in. Eleifend mi in nulla posuere sollicitudin aliquam. Eget
          aliquet nibh praesent tristique magna sit amet purus gravida. Aliquam
          sem et tortor consequat id. Vestibulum rhoncus est pellentesque elit
          ullamcorper dignissim. Aenean euismod elementum nisi quis eleifend
          quam adipiscing vitae. Sed arcu non odio euismod. Nulla malesuada
          pellentesque elit eget gravida cum sociis natoque. Arcu felis bibendum
          ut tristique et egestas. Aliquam ultrices sagittis orci a.
          Pellentesque pulvinar pellentesque habitant morbi tristique senectus
          et netus. A diam maecenas sed enim ut sem viverra.
        </p>
        <p>
          Adipiscing diam donec adipiscing tristique risus nec feugiat in
          fermentum. Id diam maecenas ultricies mi. Nisl condimentum id
          venenatis a condimentum vitae sapien. Congue mauris rhoncus aenean vel
          elit scelerisque mauris. Ac placerat vestibulum lectus mauris ultrices
          eros. Integer quis auctor elit sed vulputate mi. Vestibulum mattis
          ullamcorper velit sed ullamcorper morbi tincidunt ornare. Diam quam
          nulla porttitor massa id. Malesuada nunc vel risus commodo. Commodo
          sed egestas egestas fringilla. Leo urna molestie at elementum eu
          facilisis sed odio.
        </p>
        <p>
          Velit aliquet sagittis id consectetur purus. Ut pharetra sit amet
          aliquam id diam. Morbi tempus iaculis urna id volutpat. In est ante in
          nibh mauris cursus. Ultrices eros in cursus turpis massa tincidunt
          dui. Tincidunt lobortis feugiat vivamus at augue eget arcu dictum
          varius. Id leo in vitae turpis. Sapien pellentesque habitant morbi
          tristique. Purus non enim praesent elementum facilisis. Mi bibendum
          neque egestas congue quisque egestas diam in arcu. Vestibulum lectus
          mauris ultrices eros in cursus turpis massa tincidunt. Mattis
          ullamcorper velit sed ullamcorper morbi tincidunt ornare massa. Orci
          nulla pellentesque dignissim enim sit amet venenatis urna. Vulputate
          mi sit amet mauris commodo quis imperdiet massa. Urna id volutpat
          lacus laoreet non curabitur gravida arcu.
        </p>
        <p>
          Turpis tincidunt id aliquet risus feugiat in. Et odio pellentesque
          diam volutpat commodo sed egestas. Ac turpis egestas integer eget.
          Adipiscing elit pellentesque habitant morbi tristique senectus et.
          Mauris nunc congue nisi vitae suscipit tellus mauris a diam. Gravida
          cum sociis natoque penatibus. Ridiculus mus mauris vitae ultricies leo
          integer. At tellus at urna condimentum mattis pellentesque. Mus mauris
          vitae ultricies leo integer malesuada nunc vel. Quisque sagittis purus
          sit amet volutpat. Dictum varius duis at consectetur lorem donec massa
          sapien faucibus. Tempor commodo ullamcorper a lacus vestibulum sed.
          Nulla porttitor massa id neque. Amet venenatis urna cursus eget.
          Tincidunt eget nullam non nisi est sit amet. In nisl nisi scelerisque
          eu ultrices. Lorem mollis aliquam ut porttitor leo a diam.
        </p>
        <p>
          Porttitor eget dolor morbi non arcu risus quis varius quam. Venenatis
          lectus magna fringilla urna porttitor rhoncus dolor purus. Nibh mauris
          cursus mattis molestie a iaculis at erat. Ac feugiat sed lectus
          vestibulum mattis. Mi in nulla posuere sollicitudin aliquam ultrices
          sagittis. Sit amet purus gravida quis blandit. Habitasse platea
          dictumst vestibulum rhoncus. Amet nisl purus in mollis nunc. Ac orci
          phasellus egestas tellus rutrum tellus pellentesque eu. Nunc pulvinar
          sapien et ligula ullamcorper malesuada proin libero. Egestas integer
          eget aliquet nibh praesent tristique magna sit amet. Sed cras ornare
          arcu dui. Turpis nunc eget lorem dolor sed viverra. In hac habitasse
          platea dictumst. Pretium aenean pharetra magna ac placerat. Magna
          fringilla urna porttitor rhoncus. Magna fermentum iaculis eu non diam
          phasellus vestibulum lorem sed. Donec et odio pellentesque diam. Et
          pharetra pharetra massa massa ultricies mi. Sem integer vitae justo
          eget magna fermentum iaculis eu non.
        </p>
      </div>
      <div className="chat-container">
        <input type="text" id="chat" placeholder="Type message" />
        <button onClick={handleSendClick}>Send</button>
        <button onClick={handleShowMessagesClick}>Show messages</button>
      </div>
    </div>
  );
}

export default App;
